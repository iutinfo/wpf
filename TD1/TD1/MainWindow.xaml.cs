﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;

namespace TD1
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Tb_input_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void btn_quit_Click(object sender, RoutedEventArgs e)
        {
            // exécuté lors d'un clic sur le bouton "Quitter"

            Application.Current.Shutdown();
        }

        private void btn_ok_Click(object sender, RoutedEventArgs e)
        {
            // exécuté lors d'un clic sur le bouton "OK"

            IPHostEntry ihe;
            try
            {
                //Mettre les intructions puvant échouer

                ihe = Dns.GetHostEntry(tb_input.Text);
            }
            catch(Exception ee)
            {
                //Exécuté uniquement en cas d'echec

                MessageBox.Show(ee.Message);
                return;
            }
            //Ici tout va bien 

            lb_results.Items.Clear();
            lb_results.Items.Add("Machine : " + ihe.HostName);
            lb_results.Items.Add("\nAdresse IP : " );
            foreach(IPAddress ip in ihe.AddressList)
            {
                lb_results.Items.Add(ip.ToString());
            }

        }
    }
}
